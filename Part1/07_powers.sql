USE SuperheroesDb;


INSERT INTO Superpower
VALUES 
('Money', 'Use money to pay for cool gadgets'),
('Super Strength', 'Very strong'),
('Smart', 'Make cool gadgets'),
('Spidersense', 'Sense spiders');

INSERT INTO relationshipSuperheroPower
VALUES
(1,1),
(2,4),
(2,2),
(3,3),
(1,3);