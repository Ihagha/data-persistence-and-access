USE SuperheroesDb;

CREATE TABLE Superhero(
	Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Name nvarchar(100) NULL,
	Alias nvarchar(100) NULL,
	Origin nvarchar(100) NULL

);
CREATE TABLE Assistant(
	Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Name nvarchar(100) NULL,
);

CREATE TABLE Superpower(
	Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Name nvarchar(100) NULL,
	Description nvarchar(100) NULL
);
