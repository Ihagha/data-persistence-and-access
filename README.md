# data-persistence-and-access

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Create a database and implement a repository pattern to perform tasks on SQL server.

## Table of Contents

-   [Install](#install)
-   [Usage](#usage)
-   [Maintainers](#maintainers)
-   [Contributing](#contributing)
-   [License](#license)

## Install

For part 1:
Start SMSS (SQL Server Management Studio)

For ChinookReader in part 2:
Open project in Visual Studio with .Net, change `DataSource` in `SqlCustomerClientHelper.cs` to your server name, execute `Chinook_SqlServer_AutoIncrementPKs.sql` in SMSS and build the application.

## Usage

For part 1:
Execute queries starting with the filename starting with 01 and continue incrementing.

For ChinookReader in part 2:
In Visual Studio click run or F5.

## Maintainers

@mikaelb & @sigurd12345

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Mikael Bjerga, Sigurd Riis Haugen
