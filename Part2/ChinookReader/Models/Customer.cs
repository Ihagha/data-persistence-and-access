﻿public class Customer
{
	public Customer(int id, string firstName, string lastName, string? country, string? phone, string? postalCode, string email)
	{
		Id = id;
		FirstName = firstName;
		LastName = lastName;
		Country = country;
		Phone = phone;
		PostalCode = postalCode;
		Email = email;
	}

	public int Id { get; set; }
	public string FirstName { get; set; }
	public string LastName { get; set; }
	public string? Country { get; set; }
	public string? Phone { get; set; }
	public string? PostalCode { get; set; }
	public string Email { get; set; }


}